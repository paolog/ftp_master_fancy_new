#!/bin/sh
# Update stuff on remote
# launch from the repository home:
#   ./scripts/update.sh
#
# This file is part of ftp_master_fancy_new
# Copyright (C) 2018 Paolo Greppi <paolo.greppi@libpf.com>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

rsync -avz www/ "root@workhorse5:/var/www/html"
ssh root@workhorse5 chown -R root:www-data /var/www/html/
ssh root@workhorse5 "find /var/www/html/ -type d | xargs chmod g+x"
scp scripts/parse_queue.py root@workhorse5:.
