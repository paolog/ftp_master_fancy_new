#!/usr/bin/env python3
# coding=utf-8
#
# parses a mailing lists archive
# and stores records for ACCEPTED and REJECTED uploads in mongodb
#
# This file is part of ftp_master_fancy_new
# Copyright (C) 2018 Paolo Greppi <paolo.greppi@libpf.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import requests
import pymongo
import mailbox
import tempfile
from email.utils import parseaddr
import debian.deb822
from email.utils import parsedate_tz, mktime_tz
import datetime
import time

time_format = "%Y-%m-%dT%H:%M:%SZ"

client = pymongo.MongoClient()
db = client.ftp_master


def parse(list_name, url):
    print('%s - parsing %s' % (time.strftime(time_format, time.gmtime()), url))
    text = requests.get(url).text
    count_a = count_r = 0
    with tempfile.NamedTemporaryFile() as temp:
        temp.write(text.encode('ascii', 'ignore'))
        print('length = %d' % len(text))
        temp.flush()
        mbox = mailbox.mbox(temp.name)
        for message in mbox:
            # print(message.keys())  # ['From', 'Date', 'Subject', 'Message-ID', ...]
            # print(type(message['From']))
            if message['From'] and type(message['From']) is str:
                fro = parseaddr(message['From'].replace('\n', '').replace(' at ', '@'))[1]
                if fro == 'ftpmaster@ftp-master.debian.org':
                    subject = message['subject'].replace('\n', '')
                    parts = subject.split()
                    message_id = message['message-id'].replace('\n', '')
                    date = datetime.datetime.fromtimestamp(mktime_tz(parsedate_tz(message['date'].replace('\n', ''))))
                    if 'ACCEPTED' in subject or 'REJECTED' in subject:
                        if len(parts) > 2 and '_' in parts[2]:
                            parts2 = parts[2].split('_')
                        elif len(parts) > 1 and '_' in parts[1]:
                            parts2 = parts[1].split('_')
                        else:
                            parts2 = parts[0].split('_')
                        data = {'list_name': list_name, 'message_id': message_id, 'date': date, 'from': fro, 'subject': subject, 'body': message.get_payload(), 'source': parts2[0], 'version': parts2[1]}
                        if 'ACCEPTED' in subject:
                            changes = {}
                            ch = debian.deb822.Changes(data['body'].splitlines()[3:])
                            for c in ch:
                                key = c.lower().replace('-', '_')
                                value = ch[c]
                                if key == 'changed_by' or key == 'maintainer':
                                    value = parseaddr(value.replace(' at ', '@'))[1]
                                changes[key] = value
                            data['changes'] = changes
                            data.pop('body', None)
                    if 'REJECTED' in subject:
                        found = db.rejected.find({'message_id': data['message_id']})
                        if found.count() == 0:
                            db.rejected.insert(data)
                            count_r += 1
                    if 'ACCEPTED' in subject:
                        found = db.accepted.find({'message_id': data['message_id']})
                        if found.count() == 0:
                            db.accepted.insert(data)
                            count_a += 1
    print('%s - added %d ACCEPTED record(s) and %d REJECTED record(s)' % (time.strftime(time_format, time.gmtime()), count_a, count_r))
