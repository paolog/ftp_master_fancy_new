#!/usr/bin/env python3
# coding=utf-8
#
# bulk parses mailing list archives
#
# This file is part of ftp_master_fancy_new
# Copyright (C) 2018 Paolo Greppi <paolo.greppi@libpf.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
import mailing_list

current_day = 6
current_month = 5
current_year = 2018


def parse_monthly_archive(list_name, pattern, start_year, start_month):
    for year in range(start_year, current_year + 1):
        if year == start_year:
            first_month = start_month
        else:
            first_month = 1
        if year == current_year:
            last_month = current_month
        else:
            last_month = 12
        for month in range(first_month, last_month + 1):
            data = {'list_name': list_name, 'year': year, 'month': '%02d' % month, 'Month': datetime.date(year, month, 1).strftime("%B")}
            url = pattern % data
            mailing_list.parse(list_name, url)


def parse_weekly_archive(list_name, pattern, start_year, start_month, start_day):
    current_date = datetime.date(current_year, current_month, current_day)
    max_date = current_date - datetime.timedelta(days=7)
    date = datetime.date(start_year, start_month, start_day)
    while date <= max_date:
        data = {'list_name': list_name, 'year': date.year, 'month': '%02d' % date.month, 'day': '%02d' % date.day}
        url = pattern % data
        mailing_list.parse(list_name, url)
        date += datetime.timedelta(days=7)


parse_monthly_archive('pkg-javascript-devel', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2008, 5)

parse_weekly_archive('debian-astro-maintainers', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/Week-of-Mon-%(year)s%(month)s%(day)s.txt', 2014, 3, 17)

parse_weekly_archive('debian-iot-maintainers', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/Week-of-Mon-%(year)s%(month)s%(day)s.txt', 2016, 6, 27)

parse_monthly_archive('debian-med-packaging', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2006, 6)

parse_monthly_archive('debian-science-maintainers', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2008, 5)

parse_weekly_archive('openstack-devel', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/Week-of-Mon-%(year)s%(month)s%(day)s.txt', 2011, 11, 28)
parse_monthly_archive('openstack-devel', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2012, 3)

parse_weekly_archive('pkg-emacsen-addons', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/Week-of-Mon-%(year)s%(month)s%(day)s.txt', 2015, 8, 31)

parse_monthly_archive('pkg-fonts-devel', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2006, 3)

parse_weekly_archive('pkg-freeipa-devel', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/Week-of-Mon-%(year)s%(month)s%(day)s.txt', 2012, 3, 12)

parse_monthly_archive('pkg-games-devel', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2006, 1)

parse_monthly_archive('pkg-gnome-maintainers', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2002, 1)

parse_weekly_archive('pkg-go-maintainers', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/Week-of-Mon-%(year)s%(month)s%(day)s.txt', 2014, 1, 13)

parse_monthly_archive('pkg-grass-devel', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2004, 11)

parse_monthly_archive('pkg-haskell-maintainers', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2009, 7)

parse_monthly_archive('pkg-java-maintainers', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2003, 7)

parse_weekly_archive('pkg-matrix-maintainers', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/Week-of-Mon-%(year)s%(month)s%(day)s.txt', 2017, 7, 3)

parse_monthly_archive('pkg-multimedia-maintainers', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2007, 3)

parse_monthly_archive('pkg-nvidia-devel', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2006, 7)

parse_monthly_archive('pkg-perl-maintainers', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2004, 1)
parse_monthly_archive('pkg-postgresql-public', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2003, 10)
parse_monthly_archive('pkg-ruby-extras-maintainers', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2005, 8)
parse_monthly_archive('pkg-tcltk-devel', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2006, 9)
parse_monthly_archive('pkg-xmpp-devel', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2008, 7)
parse_monthly_archive('python-apps-team', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2007, 5)
parse_monthly_archive('python-modules-team', 'https://alioth-lists.debian.net/pipermail/%(list_name)s/%(year)s-%(Month)s.txt', 2005, 7)
